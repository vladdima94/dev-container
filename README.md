# Development container

This container has the required dependencies for developing.

## How to run it
1. Open terminal.
2. Navigate to the folder where you have the Dockerfile
3. Build docker image: `docker build --build-arg USERNAME=[USERNAME] --build-arg PASSWORD=[PASSWORD] . -t [USERNAME]:dev-container`
4. Run `docker run --privileged --memory 3072m --oom-kill-disable --user [USERNAME]:[GROUP_ID] -it -v [HOST_WORKSPACE_PATH]:/home/[USERNAME]/Workplace --name [USERNAME]-dev-container [USERNAME]:dev-container /bin/bash`

## Start docker
1. Login into the dev-container as root by running on the host: `docker exec -it --user root [CONTAINER_ID] /bin/bash`
2. Start docker by running `sudo service docker start`

docker build --build-arg USERNAME=denis --build-arg PASSWORD=denis . -t denis:dev-container

C:\Workplace\DenisWorkspace

docker run --privileged --memory 3072m --oom-kill-disable --user denis:denis -it -v C:\Workplace\DenisWorkspace:/home/denis/Workplace --name denis-dev-container denis:dev-container /bin/bash
