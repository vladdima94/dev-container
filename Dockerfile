# Using specific version of ubuntu instead of :latest
# so we don't automatically use a new version which could
# introduce breaking changes
FROM ubuntu:18.04

# Run update
RUN apt-get -y update

# Install basic tools
RUN apt-get install -y curl wget unzip git apt-utils zsh vim build-essential gcc make sudo systemd groff && \
    apt-get install locales && localedef -i en_US -f UTF-8 en_US.UTF-8

# Install Docker. This is required by AWS Cli.
RUN yes | apt-get install apt-transport-https && \
    yes | apt-get install ca-certificates && \
    yes | apt-get install gnupg-agent && \
    yes | apt-get install software-properties-common && \
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - && \
    yes | add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" && \
    apt-get update && \
    yes | apt-get install docker-ce && \
    yes | apt-get install docker-ce-cli && \
    yes | apt-get install containerd.io


# Install Amazon Cli. Keep in mind that AWS Cli requires some additional
# configuration for it to successfully execute commands (provide aws credentials,
# output format and region)
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    ./aws/install

# Install Java & Maven
RUN yes | apt-get install default-jdk && \
    yes | apt-get install maven

# Install NodeJS and Angular
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash - && \
    apt-get install -y nodejs && \
    npm install -g @angular/cli


# Define USERNAME and PASSWORD arguments so we won't have
# to hard code them in the Dockerfile and we can pass them
# as parameters to the docker build command
ARG USERNAME=username
ARG PASSWORD=password

# Create a new User and add it to the SUDO group. It is bad practice
# to connect to the container and run commands as Super User (root)
RUN useradd -ms /bin/bash $USERNAME && \
    echo $USERNAME:$PASSWORD | chpasswd && \
    usermod -aG sudo $USERNAME && \
    usermod -a -G docker $USERNAME

# Switch to the newly created User
USER $USERNAME

WORKDIR /home/$USERNAME

# Install Brew
RUN /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)" && \
    eval $($HOME/.linuxbrew/bin/brew shellenv) && \
    brew shellenv >> $HOME/.bashrc && \
    brew install gcc

# Install AWS Cli
RUN eval $($HOME/.linuxbrew/bin/brew shellenv) && \
    brew tap aws/tap && \
    brew install aws-sam-cli

# Open bash terminal
CMD /bin/bash
